package com.yasiuraroman.controller;

import com.yasiuraroman.exception.PizzaNotFoundException;
import com.yasiuraroman.exception.PizzeriaIsNotSelected;
import com.yasiuraroman.model.Pizza;
import com.yasiuraroman.model.Pizzeria;

import java.util.List;

public interface Controller {
    List<String> getPizzeriasList();
    Pizzeria selectPizzeria(String pizzeria);
    List<String> getMenuList();
    Pizza buyPizza(String pizza) throws PizzaNotFoundException, PizzeriaIsNotSelected;
}
