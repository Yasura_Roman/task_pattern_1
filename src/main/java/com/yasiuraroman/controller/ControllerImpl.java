package com.yasiuraroman.controller;

import com.yasiuraroman.exception.PizzaNotFoundException;
import com.yasiuraroman.exception.PizzeriaIsNotSelected;
import com.yasiuraroman.model.Pizza;
import com.yasiuraroman.model.Pizzeria;
import com.yasiuraroman.model.impl.FrankivskPizzeria;
import com.yasiuraroman.model.impl.LvivPizzeria;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ControllerImpl implements Controller {

    private List<Pizzeria> pizzerias;
    private Pizzeria selectedPizzeria;

    public ControllerImpl() {

        pizzerias = new LinkedList<>();
        pizzerias.add(new LvivPizzeria());
        pizzerias.add(new FrankivskPizzeria());
    }

    @Override
    public List<String> getPizzeriasList() {
        return pizzerias.stream().map(e -> e.getName()).collect(Collectors.toList());
    }

    @Override
    public Pizzeria selectPizzeria(String pizzeria) {
         selectedPizzeria = pizzerias.stream().filter(e -> e.getName().equals(pizzeria)).findFirst().get();
         return selectedPizzeria;
    }

    @Override
    public List<String> getMenuList() {
        return selectedPizzeria.priseList();
    }

    @Override
    public Pizza buyPizza(String pizza) throws PizzaNotFoundException, PizzeriaIsNotSelected {
        if (selectedPizzeria != null){
        return selectedPizzeria.sellPizza(pizza);
        }
        throw new PizzeriaIsNotSelected();
    }
}
