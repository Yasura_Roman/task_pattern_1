package com.yasiuraroman.view;

import com.yasiuraroman.controller.Controller;
import com.yasiuraroman.controller.ControllerImpl;
import com.yasiuraroman.exception.PizzaNotFoundException;
import com.yasiuraroman.exception.PizzeriaIsNotSelected;
import com.yasiuraroman.model.Pizza;
import com.yasiuraroman.model.Pizzeria;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class View {
    private Logger logger = LogManager.getLogger(View.class.getName());
    private Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Controller controller;
    private ResourceBundle menuBundle = ResourceBundle.getBundle("Menu");

    public View() {
        controller = new ControllerImpl();
        createMenu();
        initMenuCommands();
    }

    private void createMenu(){
        menu = new LinkedHashMap<>();
        SortedSet<String> sortedSet = new TreeSet<>();
        sortedSet.addAll(menuBundle.keySet());
        for (String key : sortedSet
        ) {
            menu.put(key,menuBundle.getString(key));
        }
    }

    //-------------------------------------------------------------------------
    private void initMenuCommands() {
        methodsMenu = new HashMap<>();
        methodsMenu.put("1", this::showPizzeriasList);
        methodsMenu.put("2", this::selectPizzeria);
        methodsMenu.put("3", this::showPizzeriaMenu);
        methodsMenu.put("4", this::buyPizza);
    }

    private void showPizzeriasList() {
        for (String str: controller.getPizzeriasList()
        ) {
            logger.info(str);
        }
    }

    private void selectPizzeria() {
        logger.info("select pizzeria");
        Pizzeria pizzeria = controller.selectPizzeria(input.nextLine());
        logger.info("selected pizzeria is " + pizzeria.getName());
    }

    private void showPizzeriaMenu() {
        for (String str: controller.getMenuList()
        ) {
            logger.info(str);
        }
    }

    private void buyPizza() {
        logger.info("select pizza");
        try {
            Pizza pizza = controller.buyPizza(input.nextLine());
            logger.info(pizza.getName() + "was bay");
        } catch (PizzeriaIsNotSelected pizzeriaIsNotSelected) {
            logger.error("please select pizzeria");
        } catch (PizzaNotFoundException e) {
            logger.error(e.getMessage());
        }
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
