package com.yasiuraroman;

import com.yasiuraroman.view.View;

public class Application {
    public static void main(String[] args) {
        View view = new View();
        view.show();
    }
}
