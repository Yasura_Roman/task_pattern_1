package com.yasiuraroman.model;

public enum PizzaSize {
    SMALL,
    MEDIUM,
    LARGE;
}
