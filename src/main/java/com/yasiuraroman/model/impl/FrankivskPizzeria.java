package com.yasiuraroman.model.impl;

import com.yasiuraroman.model.Pizza;
import com.yasiuraroman.model.PizzaSize;
import com.yasiuraroman.model.Pizzeria;

import java.util.HashMap;
import java.util.Map;

public class FrankivskPizzeria extends Pizzeria {

    public FrankivskPizzeria() {
        Map<Pizza,Integer> pizzaIntegerMap = new HashMap<>();

        Map<String,Integer> pizzaIngredients = new HashMap<>();

        pizzaIngredients.put("ingr1",20);
        pizzaIngredients.put("ingr2",20);
        pizzaIngredients.put("ingr3",20);
        pizzaIngredients.put("ingr4",20);
        pizzaIntegerMap.put(new Pizza("pizza1",pizzaIngredients,"dd", PizzaSize.LARGE),100);

        pizzaIngredients.clear();
        pizzaIngredients.put("ingr11",20);
        pizzaIngredients.put("ingr12",20);
        pizzaIngredients.put("ingr13",20);
        pizzaIngredients.put("ingr14",20);
        pizzaIntegerMap.put(new Pizza("pizza2",pizzaIngredients,"enouutther", PizzaSize.LARGE),100);

        pizzaIngredients.clear();
        pizzaIngredients.put("ingr21",20);
        pizzaIngredients.put("ingr22",20);
        pizzaIngredients.put("ingr23",20);
        pizzaIngredients.put("ingr24",20);
        pizzaIntegerMap.put(new Pizza("pizza3",new HashMap<String, Integer>(),"ddfada", PizzaSize.LARGE),120);

        pizzaIngredients.clear();
        pizzaIngredients.put("ingr31",20);
        pizzaIngredients.put("ingr42",20);
        pizzaIngredients.put("ingr33",20);
        pizzaIngredients.put("ingr34",20);
        pizzaIntegerMap.put(new Pizza("pizza4",new HashMap<String, Integer>(),"ddafsdfas", PizzaSize.LARGE),100);

        setName("FracnoPizza");
        setPhoneNumber("1122334455");
        setPizzaToPrice(pizzaIntegerMap);
    }

    @Override
    public String getAddress() {
        return "Ivano-Frankivsk";
    }
}
