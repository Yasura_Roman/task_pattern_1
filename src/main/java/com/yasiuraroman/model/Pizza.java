package com.yasiuraroman.model;

import java.util.Map;

public class Pizza {
    private String name;
    private Map<String,Integer> ingredients;
    private String sauce;
    private PizzaSize size;

    public Pizza() {
    }

    public Pizza(String name, Map<String, Integer> ingredients, String sauce, PizzaSize size) {
        this.name = name;
        this.ingredients = ingredients;
        this.sauce = sauce;
        this.size = size;
    }

    public String prepare(){
        return name + " pizza prepare";
    }

    public String bake(){
        return name + " pizza baking";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Integer> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Map<String, Integer> ingredients) {
        this.ingredients = ingredients;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public PizzaSize getSize() {
        return size;
    }

    public void setSize(PizzaSize size) {
        this.size = size;
    }
}

