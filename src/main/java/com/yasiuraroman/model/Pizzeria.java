package com.yasiuraroman.model;

import com.yasiuraroman.exception.PizzaNotFoundException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class Pizzeria {

    private String name;
    private String phoneNumber;
    private Map<Pizza,Integer> pizzaToPrice;

    public abstract String getAddress();

    public List<String> priseList(){
        return pizzaToPrice.entrySet().stream()
                .map( e -> e.getKey().getName() + "\t " + e.getValue())
                .collect(Collectors.toList());
    }

    public Pizza sellPizza(String pizzaName) throws PizzaNotFoundException {
        Optional<Pizza> pizza =  getPizzaToPrice().entrySet().stream()
                .filter(e -> e.getKey().getName().equals(pizzaName))
                .findFirst()
                .map(e -> e.getKey());
        return pizza.orElseThrow(PizzaNotFoundException::new);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Map<Pizza, Integer> getPizzaToPrice() {
        return pizzaToPrice;
    }

    public void setPizzaToPrice(Map<Pizza, Integer> pizzaToPrice) {
        this.pizzaToPrice = pizzaToPrice;
    }
}
